# coding: utf-8

import os
from lxml import etree


######################################## DEFINITION CONSTANTES ########################################
# 
NTR_DOC_PRT_ADR = "PAT"
TP_DOC_CRTX_PRT_ADR = "PORTADRTERMES"


######################################## DEFINITION FONCTIONS ########################################

# @param  
#       String  adr1  nom prenom porte adresse
#       String  adr2  chemin de l'index
#       String  adr3  chemin de l'index
#       String  adr4  chemin de l'index
#       String  cp  chemin de l'index
#       String  ville  chemin de l'index
#       String  nom_destinataire  nom du destinataire
#       flux_sortie  cheminFlux  chemin du flux en sortie
# @Traitement
#       
#       
#       
# @Return
#       Rien
def gene_porte_adresse_courtier(adr1, adr2, adr3, adr4, cp, ville, nom_destinataire, flux_sortie):
    # Base du flux
    base_edi = etree.Element('Editique')
    liste_contrats = etree.Element('Contrats')
    contrat = etree.Element('Contrat')

    # Definition du bloc Document qui pilote le multidoc
    document = etree.Element('Document')

    # Alimentation des balises Docucment
    nature_doc = etree.Element('Nature')
    nature_doc.text = NTR_DOC_PRT_ADR
    type_doc_cortex = etree.Element('TypeDocCortex')
    type_doc_cortex.text = TP_DOC_CRTX_PRT_ADR
    # Ajout des balises document dans le bloc document
    document.append(nature_doc)
    document.append(type_doc_cortex)

    # Definition du bloc Courtier
    courtier = etree.Element('Courtier')

    # Alimentation des balises courtiers
    courtier_nom = etree.Element('Nom')
    courtier_nom.text = nom_destinataire
    courtier_adresse1 = etree.Element('Adresse1')
    courtier_adresse1.text = adr1
    courtier_adresse2 = etree.Element('Adresse2')
    courtier_adresse2.text = adr2
    courtier_adresse3 = etree.Element('Adresse3')
    courtier_adresse3.text = adr3
    courtier_adresse4 = etree.Element('Adresse4')
    courtier_adresse4.text = adr4
    courtier_code_postal = etree.Element('CodePostal')
    courtier_code_postal.text = cp
    courtier_ville = etree.Element('Ville')
    courtier_ville.text = ville
    # Ajout des balises courtier dans le bloc courtier
    courtier.append(courtier_nom)
    courtier.append(courtier_adresse1)
    courtier.append(courtier_adresse2)
    courtier.append(courtier_adresse3)
    courtier.append(courtier_adresse4)
    courtier.append(courtier_code_postal)
    courtier.append(courtier_ville)

    # Definition du bloc PDF
    pdf = etree.Element('PDF')
    # Ceation section PDF pour le nommage du pdf
    pdf_chemin = etree.Element('Chemin')
    pdf_chemin.text = "porteAdresse_" + os.path.splitext(os.path.basename(flux_sortie))[0]
    # Ajout des balises PDF dans le bloc PDF
    pdf.append(pdf_chemin)

    # Ajout des blocs document, courtier et PDF dans le bloc contrat puis indentation des blocs

    contrat.append(document)
    contrat.append(courtier)
    contrat.append(pdf)

    liste_contrats.append(contrat)

    base_edi.append(liste_contrats)

    fd_flux_sortie = open(flux_sortie, "w", encoding='utf-8')
    fd_flux_sortie.write(etree.tostring(base_edi, encoding='utf-8', pretty_print=True).decode('utf-8'))
    fd_flux_sortie.close()
