#!python
# coding: utf-8
#

import copy
import glob
import json
import os
import shutil
import sys
import zipfile
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from lxml import etree
from rich.progress import Progress, TimeElapsedColumn, SpinnerColumn

import AOG.hcstools as hcstools
import AOG.tools as aogtools
from AOG.tools import GeneralConfig
import AOG.constants as constants
import gene_flux_porte_adresse
import maj_tab_mentions_legales
import op_fichier
from AOG.sendmailtools import Sendmail
from AOG.logvolumetries import Volumetries

with open(os.path.join(os.path.dirname(__file__), 'appsettings.json')) as json_data:
    CONFIG = json.load(json_data)

IND_EXT_SEARCH = "/*.ind"
MAIL_CECI = "Ceci est un mail automatique.\n\n"
MAIL_W_PDF = "avec fichiers PDF : "
MAIL_W_XML = "avec fichiers XML : "

parser = etree.XMLParser(resolve_entities=False, no_network=True, encoding="utf-8", remove_blank_text=True)


class TraitementTermesEditique(GeneralConfig):
    def __init__(self, type_trt, type_run, check_space_left=True):
        current_time_date = datetime.now() + relativedelta(months=2)
        self.date_traitement = current_time_date.strftime('%m_%Y')
        self.annee_traitement = current_time_date.year

        self.appname = "COGEPRINT_TERMES_" + type_trt
        
        super().__init__(self.appname)

        # Volumetries
        self.nb_pages_total = 0
        self.nb_plis_total = 0

        self.type_trt = type_trt
        self.type_run = type_run

        # Boolean pour savoir si on est sur la partie compo globale pour des portes adresse
        self.porte_adresse = False

        self.subject_mail = "[TERMES " + type_trt + "] Traitement pour la période " + self.date_traitement

        if check_space_left is True:
            try:
                aogtools.disk_space("D:", self.logwrite)
            except aogtools.NoSpaceLeft:
                sm = Sendmail()
                sm.set_high_priority()
                body = "Espace disque insuffisant pour le traitement, voir le fichier joint"

                sm.set_subject(self.subject_mail + " KO - No space left ")
                sm.add_attachment(self.log.get_log_path())
                sm.set_body(body)
                sm.go_mail()
                sys.exit(-1)

    def get_intial_files(self):
        if self.type_trt == "AIG":
            self.root_path_aig = os.path.join(CONFIG['dirs']['input_dir_AIG'], str(
                self.annee_traitement), "termes_" + str(self.date_traitement))
            # Vérification de la présence du répertoire
            if os.path.isdir(self.root_path_aig) is False:
                # self.logwrite.info("  Traitement des fichiers AIG dans : " + self.root_path_aig)
                current_time_date = datetime.now() + relativedelta(months=1)
                self.date_traitement = current_time_date.strftime('%m_%Y')
                self.annee_traitement = current_time_date.year
                self.root_path_aig = os.path.join(CONFIG['dirs']['input_dir_AIG'], str(
                    self.annee_traitement), "termes_" + str(self.date_traitement))
                self.logwrite.info("  Traitement des fichiers AIG dans : " + self.root_path_aig)

        elif self.type_trt in ["STD", "SANTE", "ATTSCO"]:
            root_path_std = CONFIG['dirs']['input_dir_STD']
            self.logwrite.info("  Traitement des fichiers STD dans : " + root_path_std)

        # Creation des répertoires de travail principaux
        self.op_tmp_dir = aogtools.get_working_directory(self.appname)

        self.logwrite.info(
            "  Initialisation de l'environnement dans : " + self.op_tmp_dir)

        for dir_path in ["entree", "sortie", "travail"]:
            createdir = os.path.join(self.op_tmp_dir, dir_path)
            self.logwrite.info("  Création du répertoire : " + createdir)
            os.makedirs(createdir, exist_ok=True)

        self.inputdir = os.path.join(self.op_tmp_dir, "entree")
        self.outputdir = os.path.join(self.op_tmp_dir, "sortie")
        self.workingdir = os.path.join(self.op_tmp_dir, "travail")

        self.logwrite.info("  Type de termes à traiter " + self.type_trt)

        if self.type_trt == "AIG":
            trouve_fichier = False
            for file in os.listdir(self.root_path_aig):
                if "Avis_Echeances_" in file and constants.CSV_EXT in file:
                    copyin = os.path.join(self.root_path_aig, file)
                    self.logwrite.info("  Récupération du fichier " + copyin)
                    copyout = os.path.join(self.inputdir, "Termes_" + file)
                    self.logwrite.info("  Enregistrement du résultat au format .csv : " + copyout)
                    shutil.copy(copyin, copyout)
                    trouve_fichier = True
                """if "~" not in file and ".xlsx" in file:
                    copyin = os.path.join(self.root_path_aig, file)
                    copyout = os.path.join(self.inputdir, file)
                    shutil.copy(copyin, copyout)
                    if "AIG" in file:
                        trouve_fichier = True
                        self.logwrite.info("  Récupération du fichier " + copyin)
                        copyout = os.path.join(
                            self.inputdir, file.replace(".xlsx", '.csv'))
                        wb = openpyxl.load_workbook(copyin)

                        # Sélection du bon onglet
                        sh = wb['AVIS']
                        index = 0
                        with open(copyout, 'w', newline="") as file_handle:
                            csv_writer = csv.writer(file_handle, delimiter=';')
                            for row in sh.iter_rows():  # generator; was sh.rows
                                # Suppression des lignes vides
                                if row[0].value is not None:
                                    csv_writer.writerow([cell.value for cell in row])
                                index = index + 1
                                # if index == 3:
                                #     break
                        self.logwrite.info(
                            "  Enregistrement du résultat au format .csv : " + copyout)"""

            if trouve_fichier is False:
                msgerror = "Pas de fichier à traiter trouvé"
                aogtools.maj_supervision(self.appname)
                raise Exception(msgerror)

        elif self.type_trt in ["STD", "SANTE", "ATTSCO"]:

            list_dates = []
            # Récupération de tous les répertoires
            for dirs in os.listdir(root_path_std):
                dir_path = os.path.join(root_path_std, dirs)
                if os.path.isdir(dir_path) is True and "2022-07-28" not in dir_path:

                    dirtmp = dirs.replace("-", "")[0:8]
                    try:
                        # Validation du format date du répertoire
                        datetime.strptime(dirtmp, '%Y%m%d')
                        date = [dirtmp, dirs]
                        if self.type_trt in ["SANTE", "ATTSCO"]:
                            if self.type_trt in dirs:
                                list_dates.append(date)
                        else:
                            list_dates.append(date)
                    except (Exception, ):
                        pass

            list_dates.sort()
            # Traitement du dernier répertoire créé
            dir_path = os.path.join(root_path_std, list_dates[-1][1])
            if self.type_trt == "STD" and self.type_run == "TEST":
                dir_path = os.path.join(dir_path, "jeux_de_tests")
            self.logwrite.info("  Copie des fichiers depuis " + dir_path)

            for files in os.listdir(dir_path):
                file = os.path.join(dir_path, files)

                if os.path.isfile(file):
                    # En mode production ou validation des fichiers de tests
                    if self.type_run != "TEST":
                        if "TEST" not in files:
                            copyout = os.path.join(self.inputdir, files)
                            shutil.copy(file, copyout)
                            self.logwrite.info("    Copie du fichier " + files)
                        else:
                            self.logwrite.warning("    On ne traite pas le fichier " +
                                                     files + " car il s'agit d'un fichier de test")
                    else:
                        if "TEST" in files:
                            copyout = os.path.join(self.inputdir, files)
                            shutil.copy(file, copyout)
                            self.logwrite.info("    Copie du fichier " + files)
                        else:
                            self.logwrite.warning("    On ne traite pas le fichier " +
                                                     files + " car il s'agit d'un run de test")

    def std_formatage_avis_echeance(self):
        self.logwrite.info("Lancement script formatage flux - termes STD")
        self.logwrite.info("Modification de l'encodage des fichiers et suppression des tabulations")
        # self.logwrite.info("--> Restructuration des flux pour HCS Platform")

        # Réencodage du fichier au bon format
        for flux_batch_encode in glob.glob(os.path.join(self.inputdir, '*.xml')):
            self.logwrite.info("    " + flux_batch_encode)
            flux_base_encode = os.path.splitext(os.path.basename(flux_batch_encode))[0]
            # on encode
            op_fichier.encodage_fichier(flux_batch_encode, 'utf-8-sig', os.path.join(
                self.workingdir, "termes_" + flux_base_encode + constants.XML_EXT), 'utf-8')
            op_fichier.remplace_car_file(os.path.join(
                self.workingdir, "termes_" + flux_base_encode + constants.XML_EXT), "utf-8", '\t', ' ')

        self.logwrite.info("    FIN Encodage UTF 8 des fichiers")

        if CONFIG["mentions_legales"]["update"] is True:
            self.logwrite.info("L'option de mise à jour des mentions légales est activée, " +
                               "le fichier MentionsLegales.tab sera mis à jour")

        for flux_sefas in glob.glob(os.path.join(self.workingdir, '*.xml')):
            self.logwrite.info("Mise à jour du fichier : " + os.path.basename(flux_sefas))
            tree = etree.parse(flux_sefas, parser)
            flux_editique = tree.getroot()
            dico_compagnies = {}
            dico_assisteurs = {}
            dico_protections_juridiques = {}
            dico_reseaux = {}
            dico_courtiers = {}
            dico_contacts = {}
            list_documents = []

            # Insertion table MentionsLegales.tab
            if CONFIG["mentions_legales"]["update"] is True:
                for mentions_legales in flux_editique.findall('MentionsLegales'):
                    for mention_legale in mentions_legales.findall('MentionLegale'):
                        try:
                            id_ml = mention_legale.find("Id").text
                            lib = mention_legale.find("Libelle").text
                            if id_ml is not None and lib is not None:
                                if id_ml != "" and lib != "":
                                    maj_tab_mentions_legales.maj_mentions_legales(id_ml, lib, CONFIG["mentions_legales"]["tab_file"])
                        except Exception as e:
                            print(e)
                    flux_editique.remove(mentions_legales)
            # Lecture du flux et mise en memoire

            # PARTIE Document
            for document in flux_editique.findall('Document'):
                try:
                    list_documents.append(copy.deepcopy(document))
                except Exception as e:
                    print(e)
                flux_editique.remove(document)

            # PARTIE COMPAGNIES
            for compagnies in flux_editique.findall('Compagnies'):
                for compagnie in compagnies.findall('Compagnie'):
                    try:
                        dico_compagnies[compagnie.find("IdCompagnie").text] = copy.deepcopy(compagnie)
                    except Exception as e:
                        print(e)
                flux_editique.remove(compagnies)

            # PARTIE ASSISTEURS
            for assisteurs in flux_editique.findall('Assisteurs'):
                for assisteur in assisteurs.findall('Assisteur'):
                    try:
                        dico_assisteurs[assisteur.find("IdAssisteur").text] = copy.deepcopy(assisteur)
                    except Exception as e:
                        print(e)
                flux_editique.remove(assisteurs)

            # PARTIE PROTECTIONS JURIDIQUES
            for protections_juridiques in flux_editique.findall('ProtectionsJuridiques'):
                for protection_juridique in protections_juridiques.findall('ProtectionJuridique'):
                    try:
                        pj = protection_juridique.find("IdProtectionJuridique").text
                        dico_protections_juridiques[pj] = copy.deepcopy(protection_juridique)
                    except Exception as e:
                        print(e)
                flux_editique.remove(protections_juridiques)

            # print(dicoProtectionsJuridiques)

            # PARTIE RESEAUX
            for reseaux in flux_editique.findall('Reseaux'):
                for reseau in reseaux.findall('Reseau'):
                    try:
                        dico_reseaux[reseau.find("IdReseau").text] = copy.deepcopy(reseau)
                    except Exception as e:
                        print(e)
                flux_editique.remove(reseaux)

            # PARTIE COURTIERS
            for courtiers in flux_editique.findall('Courtiers'):
                for courtier in courtiers.findall('Courtier'):
                    try:
                        dico_courtiers[courtier.find("IdCourtier").text] = copy.deepcopy(courtier)
                    except Exception as e:
                        print(e)
                flux_editique.remove(courtiers)

            # PARTIE CONTACTS
            for contacts in flux_editique.findall('Contacts'):
                for contact in contacts.findall('Contact'):
                    try:
                        dico_contacts[contact.find("ContactId").text] = copy.deepcopy(contact)
                    except Exception as e:
                        print(e)
                flux_editique.remove(contacts)
            # Reoganisation flux

            for contrats in flux_editique.findall('Contrats'):
                for contrat in contrats.findall('Contrat'):
                    # PARTIE DOCUMENTS
                    for document in list_documents:
                        contrat.append(copy.deepcopy(document))

                    if contrat.find("TypeDocCortex") is not None:
                        contrat.find("Document").find("TypeDocCortex").text = contrat.find("TypeDocCortex").text

                    # le deepcopy sert à eviter les problemes memoire qui n'implique l'ajout de l'objet
                    # qu'à la derniere occurence

                    # PARTIE COMPAGNIES
                    for contrat_compagnie in contrat.findall('ContratCompagnie'):
                        children = contrat_compagnie.getchildren()
                        temp_ctr_comp = copy.deepcopy(contrat_compagnie)

                        ctr_comp = copy.deepcopy(dico_compagnies[temp_ctr_comp.find('IdCompagnie').text])
                        for child in children:
                            if child.text is not None:
                                if child.tag != "IdCompagnie":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_comp.append(cpy_child)
                        contrat.remove(contrat_compagnie)
                        contrat.append(ctr_comp)

                    # PARTIE ASSISTEURS
                    for contrat_assisteur in contrat.findall('ContratAssisteur'):
                        children = contrat_assisteur.getchildren()
                        temp_ctr_assist = copy.deepcopy(contrat_assisteur)

                        ctr_comp = copy.deepcopy(dico_assisteurs[temp_ctr_assist.find('IdAssisteur').text])
                        for child in children:
                            if child.text is not None:
                                if child.tag != "IdAssisteur":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_comp.append(cpy_child)
                        contrat.remove(contrat_assisteur)
                        contrat.append(ctr_comp)

                    # PARTIE PROTECTIONS JURIDIQUES
                    for contrat_protection_juridique in contrat.findall('ContratProtectionJuridique'):
                        children = contrat_protection_juridique.getchildren()
                        temp_ctr_pj = copy.deepcopy(contrat_protection_juridique)
                        try:
                            ctr_pj = copy.deepcopy(
                                dico_protections_juridiques[temp_ctr_pj.find('IdProtectionJuridique').text])
                        except (Exception, ):
                            continue

                        for child in children:
                            if child.text is not None:
                                if child.tag != "IdProtectionJuridique":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_pj.append(cpy_child)
                        contrat.remove(contrat_protection_juridique)
                        contrat.append(ctr_pj)

                    # PARTIE RESEAUX
                    for contrat_reseau in contrat.findall('ContratReseau'):
                        children = contrat_reseau.getchildren()
                        temp_ctr_rsx = copy.deepcopy(contrat_reseau)

                        ctr_rsx = copy.deepcopy(dico_reseaux[temp_ctr_rsx.find('IdReseau').text])
                        for child in children:
                            if child.text is not None:
                                if child.tag != "IdReseau":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_rsx.append(cpy_child)
                        contrat.remove(contrat_reseau)
                        contrat.append(ctr_rsx)

                    # PARTIE COURTIERS
                    for contrat_courtier in contrat.findall('ContratCourtier'):
                        children = contrat_courtier.getchildren()
                        temp_ctr_courtier = copy.deepcopy(contrat_courtier)

                        ctr_courtier = copy.deepcopy(dico_courtiers[temp_ctr_courtier.find('IdCourtier').text])
                        for child in children:
                            if child.text is not None:
                                if child.tag != "IdCourtier":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_courtier.append(cpy_child)
                        contrat.remove(contrat_courtier)
                        contrat.append(ctr_courtier)

                    # PARTIE CONTACTS
                    for contrat_contact in contrat.findall('ContratContact'):
                        children = contrat_contact.getchildren()
                        temp_ctr_contact = copy.deepcopy(contrat_contact)

                        ctr_contact = copy.deepcopy(dico_contacts[temp_ctr_contact.find('ContactId').text])
                        for child in children:
                            if child.text is not None:
                                if child.tag != "ContactId":
                                    cpy_child = copy.deepcopy(child)
                                    ctr_contact.append(cpy_child)
                        contrat.remove(contrat_contact)
                        contrat.append(ctr_contact)

            self.logwrite.debug("  Ouverture de " + os.path.basename(flux_sefas))
            fd = open(os.path.join(self.workingdir, os.path.basename(flux_sefas)), "w", encoding='utf-8')
            self.logwrite.debug("  Ecriture de " + os.path.basename(flux_sefas))
            fd.write(etree.tostring(flux_editique, encoding='utf-8', pretty_print=True).decode("utf-8"))
            self.logwrite.debug("  Ecriture OK")

    def aig_formatage_avis_echeance(self):
        self.logwrite.info("Lancement script formatage flux - termes AIG")

        self.logwrite.info("Modification de l'encodage des fichiers et du séparateur (tab -> ;)")
        for flux_batch_encode in glob.glob(os.path.join(self.inputdir, '*.csv')):
            self.logwrite.info("    " + flux_batch_encode)
            flux_base_encode = os.path.splitext(os.path.basename(flux_batch_encode))[0]
            # on encode
            op_fichier.encodage_fichier(flux_batch_encode, 'ANSI', os.path.join(
                self.workingdir, flux_base_encode + ".csv"), 'utf-8')
            # on supprime les tabulations
            # op_fichier.remplace_car_file(os.path.join(
            #     self.workingdir, flux_base_encode + ".csv"), "utf-8", '\t', ';')
            # shutil.move(fluxBatchEncode, repFluxSauvegarde+basename(fluxBatchEncode))
        self.logwrite.info("Encodage UTF-8 terminé")

        self.logwrite.info("Conversion csv -> XML pour traitement dans HCS Designer")
        for flux_aig in glob.glob(os.path.join(self.workingdir, '*')):
            flux_base_aig = os.path.splitext(os.path.basename(flux_aig))[0]

            self.logwrite.info("Traitement flux : " +
                               os.path.basename(flux_aig))
            fd_lecture = open(flux_aig, encoding='utf-8')
            flag_premiere_ligne = True
            flag_mentions_legales = True

            # Base du flux
            base_edi = etree.Element('Editique')
            liste_contrats = etree.Element('Contrats')

            # Definition du bloc Document qui pilote le multidoc
            document = etree.Element('Document')

            nature_doc = etree.Element('Nature')
            nature_doc.text = "AE"

            type_doc_cortex = etree.Element('TypeDocCortex')
            type_doc_cortex.text = "AIG"

            document.append(nature_doc)
            document.append(type_doc_cortex)

            # Lecture ligne par ligne du fichier csv terme AIG
            for line in fd_lecture.read().splitlines():
                # if line != ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;":

                try:
                    if flag_premiere_ligne:
                        flag_premiere_ligne = False
                        continue
                    else:
                        tab_line = line.split(';')

                        branche_csv = tab_line[1]
                        produit_libelle = tab_line[4]
                        # supprimer 0 et espace devant le numéro de contrant
                        contrat_n_ctg = tab_line[8]
                        contrat_n_ctg = contrat_n_ctg.replace(" ", "")
                        contrat_n_ctg = str(int(contrat_n_ctg))
                        contrat_num_aliment = tab_line[7]
                        destinataire_nom = tab_line[9]
                        destinataire_prenom = tab_line[10]
                        destinataire_adresse = tab_line[11]
                        destinataire_adresse1 = tab_line[12]
                        destinataire_adresse2 = tab_line[13]
                        destinataire_adresse3 = tab_line[14]
                        destinataire_cp = tab_line[15]
                        destinataire_ville = tab_line[16]
                        destinataire_civilite_long = tab_line[17]

                        contrat_d_effet = tab_line[18]
                        contrat_d_echeance = tab_line[19]
                        contrat_p_ciale = tab_line[20]
                        contrat_t_hta = tab_line[21]
                        contrat_tt_an = tab_line[22]
                        contrat_prime_ttc = tab_line[24]
                        cbo_contrat_m_frac = tab_line[25]
                        cbo_iban_rum = tab_line[30]

                        # si premiere iteration alors on met a jour les mentions legales d'AIG dans la table
                        # refentielles des mentions legales
                        if flag_mentions_legales:
                            if CONFIG["mentions_legales"]["update"] is True:
                                self.logwrite.info("L'option de mise à jour des mentions légales est activée, "
                                                   "on met à jour le fichier MentionsLegales.tab")
                                flag_mentions_legales = False
                                compagnie_mentions_legales=tab_line[31]
                                maj_tab_mentions_legales.maj_mentions_legales("AIG", compagnie_mentions_legales,
                                                                              CONFIG["mentions_legales"]["tab_file"])

                        # creation du corps du contrat
                        # Definition du corps du doc
                        contrat = etree.Element('Contrat')

                        # Definitions des balises du bloc contrat
                        branche = etree.Element('Branche')
                        branche.text = branche_csv
                        contrat.append(branche)

                        produit = etree.Element('Produit')
                        produit.text = produit_libelle
                        contrat.append(produit)

                        ref_contrat = etree.Element('ReferenceContrat')
                        ref_contrat.text = contrat_n_ctg[0] + " " + contrat_n_ctg[1:4] + " " + contrat_n_ctg[4:]
                        contrat.append(ref_contrat)

                        numero_piece = etree.Element('NumeroPiece')
                        numero_piece.text = contrat_num_aliment
                        contrat.append(numero_piece)

                        date_debut_periode_garantie = etree.Element('DateDebutPeriodeGarantie')
                        # date_debut_periode_garantie.text = contrat_d_effet[8:10] + \
                        #                                    "-" + contrat_d_effet[5:7] + "-" + contrat_d_effet[0:4]
                        date_debut_periode_garantie.text = contrat_d_effet.replace("/", "-")
                        contrat.append(date_debut_periode_garantie)

                        date_fin_periode_garantie = etree.Element('DateFinPeriodeGarantie')
                        # date_fin_periode_garantie.text = contrat_d_echeance[8:10] + \
                        #                                  "-" + contrat_d_echeance[5:7] + "-" + contrat_d_echeance[0:4]
                        date_fin_periode_garantie.text = contrat_d_echeance.replace("/", "-")
                        contrat.append(date_fin_periode_garantie)

                        mode_fractionnement = etree.Element('ModeFractionnement')
                        mode_fractionnement.text = cbo_contrat_m_frac
                        contrat.append(mode_fractionnement)

                        # Description bloc Souscripteur
                        souscripteur = etree.Element('Souscripteur')

                        sous_nom = etree.Element('Nom')
                        sous_nom.text = destinataire_nom
                        souscripteur.append(sous_nom)

                        sous_prenom = etree.Element('Prenom')
                        sous_prenom.text = destinataire_prenom
                        souscripteur.append(sous_prenom)

                        sous_adr1 = etree.Element('Adresse1')
                        sous_adr1.text = destinataire_adresse
                        souscripteur.append(sous_adr1)

                        if destinataire_adresse1 != "":
                            sous_adr2 = etree.Element('Adresse2')
                            sous_adr2.text = destinataire_adresse1
                            souscripteur.append(sous_adr2)

                        if destinataire_adresse2 != "":
                            sous_adr3 = etree.Element('Adresse3')
                            sous_adr3.text = destinataire_adresse2
                            souscripteur.append(sous_adr3)

                        if destinataire_adresse3 != "":
                            sous_adr4 = etree.Element('Adresse4')
                            sous_adr4.text = destinataire_adresse3
                            souscripteur.append(sous_adr4)

                        sous_cp = etree.Element('CodePostal')
                        sous_cp.text = destinataire_cp
                        souscripteur.append(sous_cp)

                        sous_ville = etree.Element('Ville')
                        sous_ville.text = destinataire_ville
                        souscripteur.append(sous_ville)

                        sous_civ = etree.Element('Civilite')
                        sous_civ.text = destinataire_civilite_long
                        souscripteur.append(sous_civ)

                        # Description bloc Detail_primes
                        detail_primes = etree.Element('Detail_primes')

                        prime_ttc = etree.Element('PrimeTTC')
                        prime_ttc.text = contrat_p_ciale# .replace(".", ",")
                        detail_primes.append(prime_ttc)

                        prime_ht = etree.Element('PrimeHT')
                        prime_ht.text = contrat_t_hta# .replace(".", ",")
                        detail_primes.append(prime_ht)

                        prime_taxes = etree.Element('PrimeTAXES')
                        prime_taxes.text = contrat_tt_an# .replace(".", ",")
                        detail_primes.append(prime_taxes)

                        # Description bloc Coupon
                        coupon = etree.Element('Coupon')

                        rum = etree.Element('RUM')
                        rum.text = cbo_iban_rum
                        coupon.append(rum)

                        # Description bloc Echeancier
                        echeancier = etree.Element('Echeancier')

                        echeance_01_prime = etree.Element(
                            'echeance_01_prime')
                        echeance_01_prime.text = contrat_prime_ttc# .replace(".", ",")
                        echeancier.append(echeance_01_prime)

                        # Definition du bloc PDF
                        pdf = etree.Element('PDF')
                        # Ceation section PDF pour le nommage du pdf
                        pdf_chemin = etree.Element('Chemin')
                        pdf_chemin.text = "AIG_" + \
                                          contrat_n_ctg.replace(
                                              " ", "") + "_" + contrat_num_aliment
                        # Ajout des balises PDF dans le bloc PDF
                        pdf.append(pdf_chemin)

                        # Ajout des blocs du contrat
                        contrat.append(souscripteur)
                        contrat.append(detail_primes)
                        contrat.append(echeancier)
                        contrat.append(coupon)
                        contrat.append(copy.deepcopy(document))
                        contrat.append(pdf)

                        # Ajout du contrat dans la liste des contrats
                        liste_contrats.append(contrat)
                except Exception as ex:
                    self.logwrite.info(str(ex))
                    sys.exit(0)
                    pass

            fd_lecture.close()
            base_edi.append(liste_contrats)
            flux_sortie = open(os.path.join(
                self.workingdir, flux_base_aig + constants.XML_EXT), "w", encoding='utf-8')
            flux_sortie.write(etree.tostring(
                base_edi, encoding='utf-8', pretty_print=True).decode('utf-8'))
            flux_sortie.close()

            # Suppression du flux encode
            self.logwrite.info(flux_aig)
            self.logwrite.info("Suppression de : " + flux_aig)
            os.remove(flux_aig)

        self.logwrite.info("Conversion terminée")

    def retour_moteur_hcs(self, exit_code, std_out, std_err, pid, duree):
        msg = "ExitCode[" + str(exit_code) + "] / Retour[" + str(std_out) + "] / PID[" + str(pid) + "] / Duree[" + str(duree) + "]"
        if exit_code != 0 or self.is_debug_mode():
            msg = msg.replace("\\r\\n", "\n")

        if exit_code != 0:
            # Probleme detecte
            self.logwrite.error(msg)
            raise NameError(std_err)
        else:
            self.logwrite.debug(msg)

    def composition_batch(self, inputdir):
        self.logwrite.info("Composition des termes")

        os.environ["opWD"] = CONFIG["composition"]["opWD"]
        PYTHONHOME = CONFIG["composition"]["PYTHONHOME"]
        os.environ["PYTHONHOME"] = PYTHONHOME
        op_install_dir = CONFIG["composition"]["opInstallDir"]
        os.environ["opInstallDir"] = op_install_dir
        os.environ["TMP"] = "D:/HCS/Platform/home/temp"
        os.environ["opEuro"] = "WINDOWS"
        os.environ["opDoubleByte"] = "1"
        os.environ["PYTHONPATH"] = op_install_dir + ";" + PYTHONHOME + "/DLLs;" + PYTHONHOME + "/lib;" + \
                                   PYTHONHOME + "/lib/site-packages"
        os.environ["path"] = PYTHONHOME + ";" + op_install_dir + ";/bin;"
        os.environ["opTecMacroTab"] = "macros.tab," + \
                                      os.path.join(CONFIG["composition"]["hcsressourcespath"], "macros.tab")

        idxflux = 1
        listeflux = os.listdir(inputdir)
        for flux in listeflux:
            index_flux_lu = str(idxflux).zfill(2) + "/" + str(len(listeflux)).zfill(2)
            idxflux = idxflux + 1

            self.logwrite.info("Fichier à traiter (" + index_flux_lu + ") : " + flux)
            fichier_xml = os.path.basename(flux)

            key = fichier_xml.split("_")[0]
            self.path_travail = os.path.join(inputdir, "01_composition")
            os.makedirs(self.path_travail, exist_ok=True)

            paramtab = hcstools.lecture_tab(os.environ["opWD"], key, self.logwrite)
            os.environ["opAppli"] = paramtab["opAppli"]
            os.environ["opFam"] = paramtab["opFam"]

            self.logwrite.debug("opAppli : %s | opFam : %s" % (os.environ["opAppli"], os.environ["opFam"]))
            fichier_vpf = os.path.join(self.path_travail, fichier_xml.strip(constants.XML_EXT))

            command_line = CONFIG["composition"]["opInstallDir"] + os.sep + "bin" + os.sep + "pydlexec " + paramtab[
                "dataloader"] + "_py" \
                                " -i " + inputdir + os.sep + fichier_xml + \
                          " -o " + fichier_vpf + \
                          " -V withVpfID=true" + \
                          " -V librairies=" + CONFIG["composition"]["hcsressourcespath"] + \
                          " -V cheminLogos=" + CONFIG["composition"]["cheminLogos"] + \
                          " -V cheminSignatures=" + CONFIG["composition"]["cheminSignatures"] + \
                          " -V cheminParams=" + CONFIG["composition"]["cheminParams"] + \
                          " -V IDX_DATAFILE=" + fichier_xml + \
                          " -V STD5_TEMPLATE=" + paramtab["resdescid"] + constants.XML_EXT + \
                                                                         " -V STD5_WITHDATA=YES -V STD5_OUTMODE=VPF -V environnement=conception" + \
                          " -E opMaxWarning=1 -E opFam=" + os.environ["opFam"] + " -E opAppli=" + os.environ[
                              "opAppli"] + " -E opWD=" + \
                          os.environ["opWD"] + " -E opDoubleByte=" + os.environ["opDoubleByte"] + \
                          " -E opTecMacroTab=" + os.environ["opTecMacroTab"] + \
                          " -E opInstallDir=" + CONFIG["composition"]["opInstallDir"]
            # Lancement Assemblage
            self.logwrite.info("    Lancement de la composition pydlexec")
            self.logwrite.debug(command_line)
            date_debut_step = datetime.now()
            exit_code, pid, std_out, std_err = hcstools.run_command(command_line)

            self.logwrite.info("    Retour pydlexec : " + str(exit_code))

            self.retour_moteur_hcs(exit_code, std_out, std_err, pid, str(datetime.now() - date_debut_step))

            self.logwrite.info("    Lancement du techsort d'éclatement")

            self.inddir = os.path.join(inputdir, "02_indfiles")
            os.makedirs(self.inddir, exist_ok=True)

            self.pdfdir = os.path.join(inputdir, "02_pdffiles")
            os.makedirs(self.pdfdir, exist_ok=True)

            # Creation des fichiers .ind
            command_line = os.path.join(os.environ['opInstallDir'], 'bin', 'techsort') + \
                          ' -E opInstallDir=' + os.environ['opInstallDir'] + \
                          ' -E opMaxWarning=1' + \
                          ' -c ' + CONFIG["composition"]["hcsressourcespath"] + os.sep + 'techsort_termes.cmd' + \
                          ' -i ' + fichier_vpf + \
                           ' -S OUTPUTDIR=' + self.inddir + \
                           ' -R -NoTechInfo -noOp -nomerge -NamedIn -NamedOut' + \
                           ' -E opFam=' + os.environ["opFam"] + ' -E opAppli=' + os.environ["opAppli"] + ' -E opWD=' + \
                           os.environ["opWD"] + ' -E opDoubleByte=' + \
                           os.environ["opDoubleByte"]
            exit_code, pid, std_out, std_err = hcstools.run_command(command_line)
            self.logwrite.debug(command_line)
            self.logwrite.info("    Retour techsort ind : " + str(exit_code))

            self.retour_moteur_hcs(exit_code, std_out, std_err, pid, str(datetime.now() - date_debut_step))

            # Creation des PDF
            command_line = os.path.join(os.environ['opInstallDir'], 'bin', 'techsort') + \
                          ' -E opInstallDir=' + os.environ['opInstallDir'] + \
                          ' -E opMaxWarning=1' + \
                          ' -c ' + CONFIG["composition"]["hcsressourcespath"] + os.sep + 'dollar_doc_termes.cmd' + \
                          ' -i ' + fichier_vpf + \
                          ' -o ' + fichier_vpf + "_s.vpf" + \
                          ' -S OUTPUTDIR=' + self.pdfdir.replace("\\", "\\\\") + \
                          ' -S RESSOURCESDIR=' + CONFIG["composition"]["hcsressourcespath"] + \
                          ' -NamedIn -NamedOut' + \
                           ' -E opFam=' + os.environ["opFam"] + ' -E opAppli=' + os.environ["opAppli"] + ' -E opWD=' + \
                           os.environ["opWD"] + ' -E opDoubleByte=' + \
                           os.environ["opDoubleByte"]
            exit_code, pid, std_out, std_err = hcstools.run_command(command_line)

            self.logwrite.debug(command_line)

            self.logwrite.info("    Retour techsort enrichissement : " + str(exit_code))

            self.retour_moteur_hcs(exit_code, std_out, std_err, pid, str(datetime.now() - date_debut_step))

            # Copie du fichier d'index dans l'usine à courrier
            if self.porte_adresse is False:
                indexfile = hcstools.IndFile(self.appname, self.logwrite, compo_hcs=True)
                indexfile.init_files()
                indexfile.get_hc_sindexfile(fichier_vpf + ".ind")

            # Création des fichiers PDF
            command_line = os.path.join(os.environ['opInstallDir'], 'bin', 'techcodr') + \
                          " -iv " + fichier_vpf + "_s.vpf" + \
                          " -od " + fichier_vpf + "_s.pdf" + \
                          ' -DM ' + CONFIG["composition"]["hcsressourcespath"] + \
                          " -E opInstallDir=" + os.environ['opInstallDir'] + \
                           " -dn pdf_ttf2"
            # self.logwrite.info("  Rendering : creation du fichier " + file.replace(".vpf",".pdf"))
            date_debut_step = datetime.now()
            # break
            self.logwrite.debug(command_line)
            # self.logwrite.info(commandLine)
            exit_code, pid, std_out, std_err = hcstools.run_command(command_line)
            self.logwrite.info("    Retour techcodr : " + str(exit_code))
            if exit_code != 0:
                # Probleme detecte
                self.logwrite.error("Erreur detectee a la mise en protocole : ExitCode[" + str(
                    exit_code) + "] / Erreur[" + str(std_out) + str(std_err) + "] / PID[" + str(pid) + "]")
                self.logwrite.error("Rendering : ExitCode[" + str(exit_code) + "] / Retour[" + str(
                    std_out) + "] / PID[" + str(pid) + "] / Duree[" + str(
                    datetime.now() - date_debut_step) + "]")
                raise NameError(std_err)

            self.logwrite.info("    Calcul du nombre de fichiers vpf générés")
            # self.logwrite.info("Et déplacement des fichiers ind (pour perf système)")
            nbpdf = len(os.listdir(self.pdfdir))

            # Fin du script et donc de la trace
            self.logwrite.info("Fin de la partie composition / mep")

            self.nb_plis_total = nbpdf

    @staticmethod
    def get_index_col(index_file, num_col):
        colonne = ""
        try:
            fd_index = open(index_file, "r", errors='ignore')
        except (NameError, ):
            raise " pb ouverture index " + os.path.basename(index_file)
        try:
            lines = fd_index.readlines()
        except (NameError,):
            raise " pb lecture index " + os.path.basename(index_file)
        try:
            fd_index.close()
        except (NameError,):
            raise " pb fermeture index " + os.path.basename(index_file)
        colonne = lines[1].split('\t')[num_col].replace("\n", "")

        return colonne

    @staticmethod
    def move_input_rep_travail(rep_entree_lotissement, rep_traitement):
        # on encode les index
        for index_rep in glob.glob(rep_entree_lotissement + '/*.ind'):
            index = os.path.basename(index_rep)
            if index != "desktop.ind":
                fd_in = open(index_rep, 'r', encoding='utf-8-sig')
                fd_out = open(rep_traitement + "/" + index, 'w', encoding='utf-8')
                s = fd_in.read()
                fd_out.write(s)
                fd_out.close()
                fd_in.close()
                if tte.is_debug_mode():
                    os.remove(index_rep)

        for file in glob.glob(rep_entree_lotissement + "/*"):
            if tte.is_debug_mode():
                shutil.move(file, rep_traitement + "/" + os.path.basename(file))
            else:
                shutil.copy(file, rep_traitement + "/" + os.path.basename(file))

    # @param
    #       String  rep_traitement  repertoire comprenant des couples pdf / index, chaque pdf a un index ayant
    #       le même nom (par exemple a.pdf / a.ind )
    # @Traitement
    #       On genere le nom du flux type " typeDoc _numLot_NumContratPiece"
    # @Return
    #       Rien
    def renommer_pdf(self):
        self.logwrite.info("Renommage des pdf")
        for index in glob.glob(self.inddir + IND_EXT_SEARCH):
            nom_fichier_sans_extension = os.path.splitext(os.path.basename(index))[0]
            # dans le cadre de AIG on a un nommage comme ça : AIG_4091679_L000000020288,
            # sinon AVISECHEAN-FLOTTE-04F100517-02-20210303-0919
            if nom_fichier_sans_extension.split('_')[0] == "AIG":
                split_car = "_"
            else:
                split_car = "-"
            split = nom_fichier_sans_extension.split(split_car)

            # On cherche le type doc
            type_doc_split = split[0]
            # par defaut on dit que nomDoc c'est la premiere zope du nom du fichier
            nom_doc = type_doc_split
            if type_doc_split == "AIG" or type_doc_split == "AVISECHEAN":
                nom_doc = "AE"
            if type_doc_split == "CVDEFINITIVE":
                nom_doc = "CV"

            # on cherche le num de contrat
            num_contrat = split[2]

            if num_contrat == "CHAT":
                num_contrat = split[3]

            nouveau_nom = nom_doc + "_" + num_contrat

            if (os.path.isfile(self.inddir + os.sep + nouveau_nom + ".ind") or os.path.isfile(
                    self.pdfdir + os.sep + nouveau_nom + constants.PDF_EXT)):
                fichier_log_erreur = self.rep_sortie_log_erreur + '/renommetPDF.txt'

                self.logwrite.warning("PB DOUBLON - renommage PDF voir le fichier de log ")
                if not os.path.isdir(self.rep_sortie_log_erreur):
                    os.mkdir(self.rep_sortie_log_erreur)
                file_object = open(fichier_log_erreur, 'a')
                file_object.write('Doublon : nouveau ' + nouveau_nom + '\tancien :' + nom_fichier_sans_extension + '\n')
                file_object.close()
            else:
                self.logwrite.debug("Renommage du fichier " + nom_fichier_sans_extension +
                                    ".ind en " + nouveau_nom + ".ind")
                os.rename(os.path.join(self.inddir, nom_fichier_sans_extension +
                                       ".ind"), os.path.join(self.inddir, nouveau_nom + ".ind"))

                self.logwrite.debug("Renommage du fichier " + nom_fichier_sans_extension +
                                    ".pdf en " + nouveau_nom + constants.PDF_EXT)
                os.rename(os.path.join(self.pdfdir, nom_fichier_sans_extension +
                                       constants.PDF_EXT), os.path.join(self.pdfdir, nouveau_nom + constants.PDF_EXT))

    # @param
    #       String  index  chemin absolu d'un index
    # @Traitement
    #       On lit l'index puis :
    #           - on genere la partie pli de l'ensemble de la vie
    #           - Si Lotissement :
    #               - generation de la partie lotissement
    #               - Generation flux porte adresse
    # @Return
    #       chemin du flux genere
    def lecture_index(self, index):
        flag_lotissement = self.get_index_col(index, self.idxcol_lotissement)
        if flag_lotissement == "OUI":
            self.genxml_lot_unitaire(index)
        self.genxml_pli_unitaire(index)

        return "lecture fin"

    def genxml_pli_unitaire(self, index):
        self.logwrite.debug("  Pli unitaire pour : " + os.path.basename(index))
        nom_index_sans_extension = os.path.splitext(os.path.basename(index))[0]

        xml_base_pli = etree.Element('pli')
        type_envoi = etree.Element('type')
        lotissement = etree.Element('lotissement')
        regroupement = etree.Element('regroupement')
        portevignette = etree.Element('portevignette')
        lettreretour = etree.Element('lettreretour')
        carteverte = etree.Element('carteverte')
        adresse1 = etree.Element('adresse1')
        adresse2 = etree.Element('adresse2')
        adresse3 = etree.Element('adresse3')
        adresse4 = etree.Element('adresse4')
        adresse5 = etree.Element('adresse5')
        cp = etree.Element('cp')
        ville = etree.Element('ville')

        type_envoi.text = "direct"
        if self.get_index_col(index, self.idxcol_lotissement) == 'OUI':
            type_envoi.text = "lotissement"
            lotissement.text = self.get_index_col(index, self.idxcol_num_lot)

        num_contrat = nom_index_sans_extension.split('_')[1]
        regroupement.text = num_contrat

        lib_branche = self.get_index_col(index, self.idxcol_anx_branche)
        if ((lib_branche == "CYCLO" or lib_branche == "MOTO" or lib_branche == "SCOOTER") and (
                os.path.isfile(self.pdfdir + "/CV_" + num_contrat + constants.PDF_EXT))):
            portevignette.text = "1"

        mode_paiement = self.get_index_col(
            index, self.idxcol_anx_mode_paiement)
        # modif HLL : 04/02/22 - ajout de condition pour les AIG qui n'ont pas le modePaiement dans leur flux AE
        if mode_paiement == "":
            mode_paiement = 0

        if int(mode_paiement) == self.mode_paiement_cheque:
            lettreretour.text = "1"

        if os.path.isfile(self.pdfdir + "/CV_" + num_contrat + constants.PDF_EXT):
            carteverte.text = "1"

        # GESTION ADRESSE - Petite explication
        # ICI on alimente l'adresse 4 qui doit être la rue ou l'element qui defini ou distribuer le courrier
        adr_indx2 = self.get_index_col(index, self.idxcol_souscripteur_adr2)
        adr_indx3 = self.get_index_col(index, self.idxcol_souscripteur_adr3)
        adr_indx4 = self.get_index_col(index, self.idxcol_souscripteur_adr4)
        val_temp = ""
        # on verifie si l'adresse 2 est non vide puis si elle commence par un chiffre et si oui on la met
        # a la place de l'adresse 4
        if adr_indx2 != "":
            try:

                val_temp = adr_indx2
                adr_indx2 = adr_indx4
                adr_indx4 = val_temp
            except (Exception, ):
                val_temp = ""
        # Meme procedure, on verifie pour l'adresse 3 et si l'action precedente n'a pas ete concluante
        if adr_indx3 != "" and val_temp == "":
            try:

                val_temp = adr_indx3
                adr_indx3 = adr_indx4
                adr_indx4 = val_temp
            except (Exception, ):
                val_temp = ""

        # Si les deux actions precedentes n'ont menee a rien  et
        if adr_indx4 == "":
            if adr_indx2 != "":
                val_temp = adr_indx2
                adr_indx2 = adr_indx4
                adr_indx4 = val_temp
            else:
                val_temp = adr_indx3
                adr_indx3 = adr_indx4
                adr_indx4 = val_temp

        # FIN - GESTION ADRESSE

        adresse1.text = self.get_index_col(
            index, self.idxcol_souscripteur_adr1)
        adresse2.text = adr_indx2
        adresse3.text = adr_indx3
        adresse4.text = adr_indx4
        adresse5.text = self.get_index_col(
            index, self.idxcol_souscripteur_adr5)
        cp.text = self.get_index_col(
            index, self.idxcol_souscripteur_adr_cp)
        ville.text = self.get_index_col(
            index, self.idxcol_souscripteur_adr_ville)

        xml_base_pli.append(type_envoi)
        xml_base_pli.append(lotissement)
        xml_base_pli.append(regroupement)
        xml_base_pli.append(portevignette)
        xml_base_pli.append(lettreretour)
        xml_base_pli.append(carteverte)
        xml_base_pli.append(adresse1)
        xml_base_pli.append(adresse2)
        xml_base_pli.append(adresse3)
        xml_base_pli.append(adresse4)
        xml_base_pli.append(adresse5)
        xml_base_pli.append(cp)
        xml_base_pli.append(ville)

        if len(cp.text) == 5:
            chemin_flux = self.rep_traitement_pli + os.sep + num_contrat + constants.XML_EXT
            self.logwrite.debug("    flux généré : " + chemin_flux)
            if not os.path.isfile(chemin_flux):
                flux_sortie = open(chemin_flux, "w", encoding='utf-8')
                flux_sortie.write(etree.tostring(
                    xml_base_pli, encoding='utf-8', pretty_print=True).decode('utf-8'))
                flux_sortie.close()
        else:
            # TASK 42477
            # Si on a une erreur de CP alors on ne prend pas le fichier dans la production
            os.makedirs(self.rep_erreur, exist_ok=True)

            # Copie des fichiers ind et pdf dans le répertoire d'erreur
            shutil.copy(index, self.rep_erreur)
            os.remove(index)

            pdffile = os.path.join(self.pdfdir, os.path.basename(index).replace(".ind", constants.PDF_EXT))
            body = "Bonjour,\n\nFichier avec un fichier termes avec un code postal incohérent pour traitement manuel." \
                   "\n\nLe fichier ne sera pas imprimé mais archivé dans Cortex."

            sm = Sendmail()
            sm.set_subject(self.subject_mail + " Anomalie sur CP")
            sm.set_to(CONFIG["mailqualitegestion"]["to"])
            sm.add_attachment(pdffile)
            sm.set_high_priority()
            sm.set_body(body)
            sm.go_mail()

            self.logwrite.warning("Le document " + os.path.basename(pdffile) + " est incorrect pour impression (pb CP)")
            shutil.copy(pdffile, self.rep_erreur)
            os.remove(pdffile)

    # @param
    #       String  index  chemin absolu d'un index
    # @Traitement
    #       On lit l'index puis :
    #           - on genere le xml LOT relatif à cet index
    #           - on genere le flux porte adresse
    # @Return
    #       chemin du flux genere
    def genxml_lot_unitaire(self, index):
        self.logwrite.info("  Lot unitaire pour : " + os.path.basename(index))
        chemin_flux = ''
        if self.get_index_col(index, self.idxcol_lotissement) == 'OUI':
            xml_base_lot = etree.Element('pli')
            lotissement = etree.Element('lotissement')
            adresse1 = etree.Element('adresse1')
            adresse2 = etree.Element('adresse2')
            adresse3 = etree.Element('adresse3')
            adresse4 = etree.Element('adresse4')

            cp = etree.Element('cp')
            ville = etree.Element('ville')

            num_lot = self.get_index_col(index, self.idxcol_num_lot)
            lotissement.text = num_lot

            # GESTION ADRESSE
            # ICI on alimente l'adresse 4 qui doit être la rue ou l'element qui defini ou distribuer le courrier
            adr_indx2 = self.get_index_col(index, self.idxcol_lot_adr2)
            adr_indx3 = self.get_index_col(index, self.idxcol_lot_adr3)
            adr_indx4 = self.get_index_col(index, self.idxcol_lot_adr4)
            val_temp = ""
            # on verifie si l'adresse 2 est non vide puis si elle commence par un chiffre et si oui on la met
            # a la place de l'adresse 4
            if adr_indx2 != "":
                try:
                    val_temp = adr_indx2
                    adr_indx2 = adr_indx4
                    adr_indx4 = val_temp
                except (Exception, ):
                    val_temp = ""

            # Meme procedure, on verifie pour l'adresse 3 et si l'action precedente n'a pas ete concluante
            if adr_indx3 != "" and val_temp == "":
                try:
                    val_temp = adr_indx3
                    adr_indx3 = adr_indx4
                    adr_indx4 = val_temp
                except (Exception, ):
                    val_temp = ""

            # Si les deux actions precedentes n'ont menee a rien  et
            if adr_indx4 == "":
                if adr_indx2 != "":
                    val_temp = adr_indx2
                    adr_indx2 = adr_indx4
                    adr_indx4 = val_temp
                else:
                    val_temp = adr_indx3
                    adr_indx3 = adr_indx4
                    adr_indx4 = val_temp

            # FIN - GESTION ADRESSE

            adresse1.text = self.get_index_col(index, self.idxcol_lot_adr1)
            adresse2.text = adr_indx2
            adresse3.text = adr_indx3
            adresse4.text = adr_indx4
            cp.text = self.get_index_col(index, self.idxcol_lot_cp)
            ville.text = self.get_index_col(index, self.idxcol_lot_ville)

            xml_base_lot.append(lotissement)
            xml_base_lot.append(adresse1)
            xml_base_lot.append(adresse2)
            xml_base_lot.append(adresse3)
            xml_base_lot.append(adresse4)
            xml_base_lot.append(cp)
            xml_base_lot.append(ville)

            # generation flux porte adresse
            self.gen_flux_port_adresse(index, num_lot)

            # generation flux lot unitaire
            chemin_flux = self.rep_traitement_lot + os.sep + num_lot + constants.XML_EXT
            if not os.path.isfile(chemin_flux):
                flux_sortie = open(chemin_flux, "w", encoding='utf-8')
                flux_sortie.write(etree.tostring(
                    xml_base_lot, encoding='utf-8', pretty_print=True).decode('utf-8'))
                flux_sortie.close()

        return chemin_flux

    # @Traitement
    #       On lit l'ensemble des fichiers xlm pli
    #       On les fusionne et on les mets dans un xml
    # @Return
    #       chemin du flux genere
    def fusion_xml_pli(self):
        plis_xml = self.rep_traitement + "/regroupement.xml"
        self.logwrite.info("  Fusion des XML pli dans le fichier : " + plis_xml)
        fd_plis_xml_w = open(plis_xml, "w", encoding='utf-8')
        fd_plis_xml_w.write("<plis>\n")
        # print(self.rep_traitement_pli)
        for xml in glob.glob(self.rep_traitement_pli + "/*"):
            # print(xml)
            fd_lecture = open(xml, "r", encoding='utf-8')
            for line_pli in fd_lecture.read().splitlines():
                fd_plis_xml_w.write(line_pli + '\n')
        fd_plis_xml_w.write("</plis>")
        fd_plis_xml_w.close()

    # @Traitement
    #       On lit l'ensemble des fichiers xlm lot
    #       On les fusionne et on les mets dans un xml
    # @Return
    #       chemin du flux genere
    def fusion_xml_lot(self):
        self.logwrite.info("  Fusion des XML lot")
        lots_xml = self.rep_traitement + "/lotissement.xml"
        fd_lots_xml_w = open(lots_xml, "w", encoding='utf-8')
        fd_lots_xml_w.write("<plis>\n")

        for xml in glob.glob(self.rep_traitement_lot + "/*"):
            fd_lecture = open(xml, "r", encoding='utf-8')
            for line_lot in fd_lecture.read().splitlines():
                fd_lots_xml_w.write(line_lot + '\n')

        fd_lots_xml_w.write("</plis>")
        fd_lots_xml_w.close()

    # @param
    #       String  index  chemin absolu d'un index a destination d'un lotissement
    #       String  num_lot  ID du lot pour nommage du flux
    # @Traitement
    #       On genère un flux avec les infos de l'adresse.
    #       On crée le flux dans le repertoire des flux porte adresse
    # @Return
    #       chemin du flux genere
    def gen_flux_port_adresse(self, index, num_lot):
        self.logwrite.info("Génération des flux porte adresse")
        adresse1 = self.get_index_col(index, self.idxcol_lot_adr1)
        adresse2 = self.get_index_col(index, self.idxcol_lot_adr2)
        adresse3 = self.get_index_col(index, self.idxcol_lot_adr3)
        adresse4 = self.get_index_col(index, self.idxcol_lot_adr4)
        adresse_cp = self.get_index_col(index, self.idxcol_lot_cp)
        adresse_ville = self.get_index_col(index, self.idxcol_lot_ville)
        adresse_nom_destinataire = self.get_index_col(
            index, self.idx_col_lot_nom_dest)

        # le nom du flux est tout simple termes + nom du lot.
        # le mot cle termes est la cle primaire dans la table de parametrage "D:\params\paramTagOPWD.tab"
        # qui permet de generer le pdf via le bon tag
        nom_flux = self.cle_op_wd_termes_sefas + "_" + num_lot + constants.XML_EXT
        flux_sortie = self.rep_traitement_lot_porte_adr + os.sep + nom_flux

        # on appelle la fonction gene_porte_adresse_courtier() du script gene_flux_porte_adresse.py pour generer
        # le flux du porte adresse
        gene_flux_porte_adresse.gene_porte_adresse_courtier(
            adresse1, adresse2, adresse3, adresse4, adresse_cp, adresse_ville, adresse_nom_destinataire, flux_sortie)
        return flux_sortie

    # @param
    #       NIET
    # @Traitement
    #       Compose les flux du repertoire des flux porte adresse (gen_flux_port_adresse) dans
    #       le repertoire de traitement
    # @Return
    #       Un message pas vraiment utile
    def gen_pdf_port_adresse(self):
        self.logwrite.info("  Génération des porte adresse PDF")
        self.pdfdirglobal = self.pdfdir
        self.inddirglobal = self.inddir
        self.composition_batch(self.rep_traitement_lot_porte_adr)

    def del_ind_rep_trt(self):
        self.logwrite.info(
            "  Suppression des fichiers inutiles (.ind, .vpf) dans " + self.rep_traitement)
        for index_rep in glob.glob(self.rep_traitement + '/*.ind'):
            os.remove(index_rep)
            self.logwrite.debug("    Suppression du fichier : " + index_rep)
        for index_rep in glob.glob(self.rep_traitement + '/*.vpf'):
            os.remove(index_rep)
            self.logwrite.debug("    Suppression du fichier : " + index_rep)

    def zip_all(self):
        self.logwrite.info("  Compression des fichiers contenus dans " + self.pdfdir)

        zip_file_open = zipfile.ZipFile(self.nom_archive_zip_cogeprint, "w")
        self.logwrite.info("  Ouverture du fichier zip : " +
                           self.nom_archive_zip_cogeprint)

        self.nb_pdf_zip = 0
        self.nb_xml_zip = 0

        if self.type_trt in ["STD", "SANTE", "ATTSCO"]:
            lstreps = [self.rep_traitement, self.pdfdir, self.pdfdirglobal]
        else:  # AIG
            lstreps = [self.rep_traitement, self.pdfdir]

        for dir in lstreps:
            self.logwrite.info("  Parcours du répertoire : " + dir.strip())
            with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(),) as progress:
                self.nb_plis_total = len(os.listdir(dir))
                if tte.is_debug_mode():
                    task = progress.add_task("Comp. COGEPRINT", total=self.nb_plis_total)

                for files in os.listdir(dir):
                    # create complete filepath of file in directory
                    file_path = os.path.join(dir, files)
                    # Add file to zip
                    if (files != self.nom_archive_zip_cogeprint) and os.path.isfile(file_path) is True:
                        zip_file_open.write(file_path, os.path.basename(file_path))
                        self.logwrite.debug("    Ajout du fichier : " + files)

                        extension = os.path.splitext(os.path.basename(file_path))[1]
                        if extension == constants.PDF_EXT:
                            # self.logwrite.debug(os.path.basename(filePath))
                            self.nb_pdf_zip = self.nb_pdf_zip + 1
                        elif extension == constants.XML_EXT:
                            self.nb_xml_zip = self.nb_xml_zip + 1
                    if tte.is_debug_mode():
                        progress.advance(task)

        zip_file_open.close()
        self.logwrite.info("  Fermeture du fichier zip : " +
                           self.nom_archive_zip_cogeprint)

    def copie_zip(self):
        # ZIP pour imprimeur
        termesoutputdir = CONFIG['dirs']['output_dir']
        os.makedirs(termesoutputdir, exist_ok=True)

        shutil.copy(self.nom_archive_zip_cogeprint, termesoutputdir)
        self.output_cogeprint = os.path.join(termesoutputdir, os.path.basename(self.nom_archive_zip_cogeprint))
        self.logwrite.info("  Copie du fichier zip vers : " +
                           self.output_cogeprint)

        # Zip pour archive
        termesarchivedir = CONFIG['dirs']['archive_dir']
        os.makedirs(termesarchivedir, exist_ok=True)

        shutil.copy(self.nom_archive_zip_cogeprint, termesarchivedir)
        self.archive_zip_cogeprint = os.path.join(termesarchivedir, os.path.basename(self.nom_archive_zip_cogeprint))
        self.logwrite.info("  Copie du fichier zip (archive) vers : " +
                           self.archive_zip_cogeprint)

    def lotissement_cogeprint(self):
        self.logwrite.info("Initialisation des répertoires")

        self.porte_adresse = True

        rep_entree_lotissement = os.path.join(self.workingdir, "02_techsort")

        self.rep_erreur = os.path.join(self.workingdir, "04_erreur")

        self.rep_traitement = os.path.join(self.workingdir, "03_lotissement")
        self.rep_traitement_pli = os.path.join(self.rep_traitement, "PLI")
        self.rep_traitement_lot = os.path.join(self.rep_traitement, "LOTISSEMENT")
        self.rep_traitement_lot_porte_adr = os.path.join(self.rep_traitement, "LOT_PORTE_ADR")
        self.logwrite.info("    rep_traitement : " + self.rep_traitement)
        self.logwrite.debug("    rep_traitement_pli : " + self.rep_traitement_pli)
        self.logwrite.debug("    rep_traitement_lot : " + self.rep_traitement_lot)
        self.logwrite.debug("    rep_traitement_lot_porte_adr : " + self.rep_traitement_lot_porte_adr)

        self.rep_sortie_log_erreur = os.path.join(self.rep_traitement, "ERREUR")
        self.logwrite.debug("    rep_sortie_log_erreur : " + self.rep_sortie_log_erreur)

        # Repertoires de lotissement
        '''rep_lotissement = "Lotissement"
        rep_hors_lot = "direct"
        rep_porte_vignette = "2roues"
        rep_hors_porte_vignette = "hors2roues"
        rep_lettre_retour = "HPA"
        rep_hors_lettre_retour = "PA"'''

        # Valeur flux
        # /!\ CECI N'EST PAS UNE INDICATION DE COLONNE MAIS DE VALEUR A TESTER
        self.mode_paiement_cheque = 2

        # Colonne index
        # INDX_NUM_COL_NOM_FICHIER_SORTIE = 2
        # INDX_NUM_COL_CONTRAT = 8
        self.idxcol_anx_branche = 9
        self.idxcol_lotissement = 19
        self.idxcol_num_lot = 20
        self.idxcol_anx_mode_paiement = 18
        self.idxcol_lot_adr1 = 21
        self.idxcol_lot_adr2 = 22
        self.idxcol_lot_adr3 = 23
        self.idxcol_lot_adr4 = 24
        self.idxcol_lot_cp = 25
        self.idxcol_lot_ville = 26
        self.idx_col_lot_nom_dest = 27

        self.idxcol_souscripteur_adr1 = 33
        self.idxcol_souscripteur_adr2 = 34
        self.idxcol_souscripteur_adr3 = 35
        self.idxcol_souscripteur_adr4 = 36
        self.idxcol_souscripteur_adr5 = 37
        self.idxcol_souscripteur_adr_cp = 38
        self.idxcol_souscripteur_adr_ville = 39
        # # Valeur flux
        # self.mode_paiement_cheque = 2

        # # Colonne index
        # INDX_NUM_COL_NOM_FICHIER_SORTIE = 1
        # INDX_NUM_COL_CONTRAT = 6
        # self.idxcol_anx_branche = 7
        # self.idxcol_lotissement = 17
        # self.idxcol_num_lot = 18
        # self.idxcol_anx_mode_paiement = 16
        # self.idxcol_lot_adr1 = 19
        # self.idxcol_lot_adr2 = 20
        # self.idxcol_lot_adr3 = 21
        # self.idxcol_lot_adr4 = 22
        # self.idxcol_lot_cp = 23
        # self.idxcol_lot_ville = 24
        # self.idx_col_lot_nom_dest = 25
        # INDX_NUM_COL_REF_PAPIER = 30
        # self.idxcol_souscripteur_adr1 = 31
        # self.idxcol_souscripteur_adr2 = 32
        # self.idxcol_souscripteur_adr3 = 33
        # self.idxcol_souscripteur_adr4 = 34
        # self.idxcol_souscripteur_adr5 = 35
        # self.idxcol_souscripteur_adr_cp = 36
        # self.idxcol_souscripteur_adr_ville = 37

        # nommage flux
        self.cle_op_wd_termes_sefas = "termes"

        # Zip Final
        self.nom_archive_zip_cogeprint = os.path.join(
            self.outputdir, "Impression_AOG_" + self.appname + "_" + aogtools.get_time_stamp() + ".zip")

        self.logwrite.info("Création des répertoires de travail")

        try:
            if os.path.isdir(self.rep_traitement) is True:
                shutil.rmtree(self.rep_traitement)

            os.makedirs(self.rep_traitement, exist_ok=True)
            os.makedirs(self.rep_traitement_pli, exist_ok=True)
            os.makedirs(self.rep_traitement_lot, exist_ok=True)
            os.makedirs(self.rep_traitement_lot_porte_adr, exist_ok=True)
        except (Exception, ):
            self.logwrite.error("Erreur lors de la création des répertoires")
            raise

        self.move_input_rep_travail(rep_entree_lotissement, self.rep_traitement)

        self.renommer_pdf()

        for index in glob.glob(self.inddir + IND_EXT_SEARCH):
            self.lecture_index(index)

        self.logwrite.info("Préparation des index intermédiaires")
        self.fusion_xml_pli()
        self.fusion_xml_lot()

        self.gen_pdf_port_adresse()

        self.del_ind_rep_trt()

        self.zip_all()

        self.copie_zip()

    def lotissement_branche(self):
        self.idxcol_anx_branche = 9

        self.logwrite.info("Parcours des fichiers .ind pour le lotissement des AIG")
        for index in glob.glob(self.inddir + IND_EXT_SEARCH):
            lib_branche = self.get_index_col(index, self.idxcol_anx_branche)
            nom_fichier_sans_extension = os.path.splitext(os.path.basename(index))[0]
            branchedir = os.path.join(self.pdfdir, lib_branche)
            # si le repertoire du numero de lot n'existe pas on le cree
            if not os.path.isdir(branchedir):
                self.logwrite.info("   Création du lot : " + lib_branche)
                os.mkdir(branchedir)

            shutil.move(os.path.join(self.pdfdir, nom_fichier_sans_extension + constants.PDF_EXT),
                        os.path.join(branchedir, nom_fichier_sans_extension + constants.PDF_EXT))
            os.remove(os.path.join(self.inddir, nom_fichier_sans_extension + constants.IND_EXT))

        self.nom_archive_aig = "Archive_TERMES_AIG_" + aogtools.get_time_stamp() + constants.ZIP_EXT
        self.logwrite.info("   Nom du fichier zip : " + self.nom_archive_aig)

        pwd = os.getcwd()
        os.chdir(self.pdfdir)

        zip_file_name = zipfile.ZipFile(self.nom_archive_aig, "w")
        self.logwrite.info("   Ouverture du zip dans le répertoire : " + self.pdfdir)

        with Progress(SpinnerColumn(), *Progress.get_default_columns(), TimeElapsedColumn(),) as progress:
            if tte.is_debug_mode():
                task = progress.add_task("Arch AIG", total=self.nb_plis_total)

            for folder_name, subfolders, filenames in os.walk("./"):
                for filename in filenames:

                    # create complete filepath of file in directory
                    file_path = os.path.join(folder_name, filename)
                    # Add file to zip
                    if filename != self.nom_archive_aig:
                        zip_file_name.write(file_path, file_path)
                    if tte.is_debug_mode():
                        progress.advance(task)
        zip_file_name.close()

        if tte.is_env_prod() is True:
            self.nom_archive_aig_full = os.path.join(self.root_path_aig, self.nom_archive_aig)
        else:
            termesoutputdir = CONFIG['dirs']['output_dir']
            os.makedirs(termesoutputdir, exist_ok=True)

            self.nom_archive_aig_full = os.path.join(termesoutputdir, self.nom_archive_aig)

            self.logwrite.info("   Déplacement du fichier zip vers  : " + self.nom_archive_aig_full)
        shutil.move(self.nom_archive_aig, self.nom_archive_aig_full)
        os.chdir(pwd)

    def purge_working_dir(self):
        if CONFIG['purge'] is True:
            self.logwrite.info("Début de suppression du répertoire : " + self.op_tmp_dir)
            shutil.rmtree(self.op_tmp_dir)
            self.logwrite.info("Fin de suppression")
        else:
            self.logwrite("La purge automatique est désactivée")

    def archivage(self):
        archivagedir = "D:\\traitementPostProd\\transfertStockage\\entree"

        self.logwrite.info("Archivage")
        self.logwrite.info("    Nettoyage du répertoire de travail pour l'archivage")

        self.logwrite.info("    Copie des fichiers ind pour archivage")
        for file in os.listdir(self.inddirglobal):
            copyfile = os.path.join(self.inddirglobal, file)
            outputfile = os.path.join(archivagedir, file)
            if os.path.isfile(outputfile) is True:
                os.remove(outputfile)
            shutil.move(copyfile, outputfile)

        self.logwrite.info("    Copie des fichiers pdf pour archivage")
        for file in os.listdir(self.pdfdirglobal):
            copyfile = os.path.join(self.pdfdirglobal, file)
            outputfile = os.path.join(archivagedir, file)
            if os.path.isfile(outputfile) is True:
                os.remove(outputfile)
            shutil.move(copyfile, outputfile)

        self.logwrite.info("    Copie des fichiers en erreur")
        if os.path.isdir(self.rep_erreur) is True:
            for file in os.listdir(self.rep_erreur):
                copyfile = os.path.join(self.rep_erreur, file)
                outputfile = os.path.join(archivagedir, file)
                if os.path.isfile(outputfile) is True:
                    os.remove(outputfile)
                shutil.move(copyfile, outputfile)
        else:
            self.logwrite.info("    Pas de fichier à traiter")

        self.logwrite.info("Fin de l'archivage")

    def get_output_filename(self, partenaire, terme):
        return str("RAPPORT_TERMES" + "_" + partenaire + "_" + terme + ".csv")

    def log_volumetries(self):
        try:
            contrat_type = {}
            liste_rapports = CONFIG["rapports"]
            tmp_dir = tte.get_tmp_dir()

            today = datetime.today()
            date_jour = str(today.strftime("%Y-%m-%d"))
            date_depot = str((today + timedelta(days=1)).strftime("%Y-%m-%d"))
            date_envoie = str((today + timedelta(days=7)).strftime("%Y-%m-%d"))

            for file in os.listdir(self.workingdir):
                complete = os.path.join(self.workingdir, file)
                # On exclut les CV pour ne compter que les AE
                if os.path.isfile(complete) is True and "_CV_" not in file:
                    self.logwrite.info("Traitement du fichier " + complete)

                    if self.type_trt == "ATTSCO":
                        mois = 14
                        annee = file.split("_")[4].replace(".xml", "")
                    if self.type_trt == "SANTE":
                        mois = 13
                        # annee = file.split("_")[4].replace(".xml", "")
                        annee = "2023"
                    else:
                        mois = int(file.split("_")[4])
                        annee = file.split("_")[5]

                    try:
                        suffix_termes = self.type_trt + "_" + self.type_run + "_" + annee + "_" + file.split("_")[4]
                    except:
                        suffix_termes = self.type_trt + "_" + self.type_run + "_" + annee

                    self.logwrite.info("suffix_termes " + suffix_termes)
                    for partenaire in liste_rapports:
                        rapport_file = os.path.join(tmp_dir, self.get_output_filename(partenaire["nom"], suffix_termes))
                        tte.logwrite.info("Création du fichier " + rapport_file)

                        if os.path.isfile(rapport_file) is False:
                            new_fichier = open(rapport_file, 'w')
                            new_fichier.write("Produit;Contrat;Date effet;Date Terme;Date Dépot;Date Envoie (approx)\n")
                            new_fichier.close()

                    flux_sefas = os.path.join(self.workingdir, file)
                    tree = etree.parse(flux_sefas, parser)
                    flux_editique = tree.getroot()
                    for contrats in flux_editique.findall('Contrats'):
                        for contrat in contrats.findall('Contrat'):
                            produit = contrat.find("Produit").text
                            if produit in contrat_type:
                                contrat_type[produit] = contrat_type[produit] + 1
                            else:
                                contrat_type[produit] = 1

                            for partenaire in liste_rapports:
                                for lst_produits in partenaire['produits']:
                                    if lst_produits in produit:
                                        numero_contrat = contrat.find("NumeroAdhesion").text
                                        date_effet = contrat.find("DateDebutPeriodeGarantie").text
                                        line = (produit + ";" + numero_contrat + ";" + date_effet + ";" + date_jour +
                                                ";" + date_depot + ";" + date_envoie)
                                        rapport_file = os.path.join(tmp_dir,
                                                                    self.get_output_filename(partenaire["nom"], suffix_termes))
                                        actual_fichier = open(os.path.join(tmp_dir, rapport_file), 'a')
                                        actual_fichier.write(line + "\n")
                                        actual_fichier.close()

            # Enregistrmeent des volumétries
            lv = Volumetries("TERMES_PRODUIT", self.logwrite, year=annee, env=self.env)
            self.logwrite.info("Ecriture des infos pour le mois numéro " + str(mois).zfill(2))

            for item in contrat_type:
                for i in range(3, 250):
                    # L'entrée est trouvée, on renseigne la valeur
                    if item == lv.ws.cell(i, 2).value:
                        # Décalage du quantième du jour et placement de la bonne valeur
                        self.logwrite.debug(item + " - " + str(contrat_type[item]))
                        lv.ws.cell(i, 2 + mois).value = contrat_type[item]
                        break

                    # L'entrée n'existe pas, on la crée
                    elif lv.ws.cell(i, 2).value is None:
                        lv.ws.cell(i, 2).value = item
                        self.logwrite.debug(item + "-" + str(contrat_type[item]))
                        lv.ws.cell(i, 2 + mois).value = contrat_type[item]
                        break
            lv.closefile()

            for partenaire in liste_rapports:
                if partenaire["envoie"] is True:
                    rapport_file = os.path.join(tmp_dir, self.get_output_filename(partenaire["nom"], suffix_termes))
                    if os.path.isfile(rapport_file):
                        sm = Sendmail()
                        body = "Voici le rapport pour la production pour le partenaire " + partenaire["nom"]
                        if tte.is_env_prod() is True:
                            sm.set_to(partenaire["destinataire"])
                        sm.set_subject("Rapport des termes " + suffix_termes + " pour " + partenaire["nom"])
                        sm.add_attachment(rapport_file)
                        sm.set_body(body)
                        sm.go_mail()
                        self.logwrite.info("Envoie du mail de recap pour " + partenaire["nom"])

                        os.remove(rapport_file)
        except Exception as ex:
            self.logwrite.error(str(ex))


if __name__ == "__main__":
    parser = etree.XMLParser(resolve_entities=False, no_network=True, encoding="utf-8", remove_blank_text=True)
    os.system("cls")
    try:
        typeTermes = sys.argv[1]
    except (Exception, ):
        typeTermes = "AIG"

    try:
        typeRun = sys.argv[2]
    except (Exception,):
        typeRun = "STD"

    try:
        tte = TraitementTermesEditique(typeTermes, typeRun)
        tte.logwrite.info("")
        tte.logwrite.info("Partie 1 - Initialisation")
        tte.get_intial_files()
        # Standard
        if typeTermes in ["STD", "SANTE", "ATTSCO"]:
            # PreProd
            tte.logwrite.info("")
            tte.logwrite.info("Partie 2 - Pré production")
            tte.std_formatage_avis_echeance()

            # Production
            tte.logwrite.info("")
            tte.logwrite.info("Partie 3 - Production des documents")
            tte.composition_batch(tte.workingdir)

            # PostProduction
            tte.logwrite.info("")
            tte.logwrite.info("Partie 4 - Post production / Compression et mise à disposition")
            tte.lotissement_cogeprint()

            tte.logwrite.info("Traitement terminé")

            tte.archivage()

            # Mail Equipe Editique DSI
            sm = Sendmail()
            sm.set_low_priority()
            body = MAIL_CECI
            body = body + "Traitement des termes terminé sans erreur" + "\n"
            body = body + "\n"
            body = body + "Fichier ZIP : " + tte.nom_archive_zip_cogeprint + "\n"
            body = body + MAIL_W_PDF + str(tte.nb_pdf_zip) + "\n"
            body = body + MAIL_W_XML + str(tte.nb_xml_zip) + "\n"

            sm.set_subject(tte.subject_mail + " OK ")
            sm.add_attachment(tte.log.get_log_path())
            sm.set_body(body)
            sm.go_mail()

            # Mail DSI Team Termes
            smtf = Sendmail()
            smtf.set_subject(tte.subject_mail + " OK ")
            smtf.set_to(CONFIG['mailtechfactory']['to'])
            body = MAIL_CECI
            body = body + "Le traitement des termes standard est arrivé à son terme." + "\n\n"
            body = body + "Les documents ont été remis pour impression." + "\n"
            smtf.set_body(body)
            smtf.go_mail()

            # Mail Cogeprint
            # if tte.is_env_prod() is True:
            #     smcogeprint = Sendmail()
            #     body = MAIL_CECI
            #     body = body + "Bonjour,\n\nLes termes " + typeTermes + " sont disponibles sur le répertoire d'échange" \
            #                                                            " à l'adresse suivante : " + tte.nom_archive_zip_cogeprint + "\n\n"
            #     body = body + MAIL_W_PDF + str(tte.nb_pdf_zip) + "\n"
            #     body = body + MAIL_W_XML + str(tte.nb_xml_zip) + "\n"
            #     smcogeprint.set_body(body)
            #     smcogeprint.set_to(CONFIG['mailcogeprint']['cogeprintto'])
            #     smcogeprint.set_subject(tte.subject_mail + " - Dépôt zip pour traitement ", externe=True)
            #     smcogeprint.go_mail()
            tte.log_volumetries()

        # AIG
        elif typeTermes == "AIG":
            # PreProd
            tte.logwrite.info("")
            tte.logwrite.info("Partie 2 - Pré production")
            tte.aig_formatage_avis_echeance()

            # Production
            tte.logwrite.info("")
            tte.logwrite.info("Partie 3 - Production des documents")
            tte.composition_batch(tte.workingdir)

            # PostProduction
            tte.logwrite.info("")
            tte.logwrite.info("Partie 4 - Post production / Compression et mise à disposition")
            shutil.copytree(tte.pdfdir, tte.pdfdir + "_cgprint")
            shutil.copytree(tte.inddir, tte.inddir + "_cgprint")
            tte.lotissement_branche()
            tte.logwrite.info("")

            shutil.rmtree(tte.pdfdir)
            shutil.rmtree(tte.inddir)
            shutil.move(tte.pdfdir + "_cgprint", tte.pdfdir)
            shutil.move(tte.inddir + "_cgprint", tte.inddir)
            tte.lotissement_cogeprint()

            tte.logwrite.info("Traitement terminé")
            tte.logwrite.info("")

            sm = Sendmail()
            sm.set_low_priority()
            body = "Traitement des termes terminé sans erreur" + "\n"
            body = body + "\n"

            # En cas de traitement complet, on met à jour le fichier de volumétries
            # tte.logwrite.warning("Ecriture des volumétries", ENV)
            # vol = Volumetries(CONFIG["report"]["classeur"], tte.logwrite)
            # vol.startarray(CONFIG["report"]["coldeb"], CONFIG["report"]["rowdeb"], CONFIG["report"]["yeardeb"])
            # vol.setvalue(int(tte.date_traitement[3:7]), int(tte.date_traitement[0:2]), 1, tte.nb_pages_total)
            # vol.closefile()

            body = body + "Fichier Archive : " + tte.nom_archive_aig_full + "\n"
            body = body + "Fichier Cogeprint : " + tte.nom_archive_zip_cogeprint + "\n"
            body = body + "  avec fichiers PDF : " + str(tte.nb_pdf_zip) + "\n"
            body = body + "  avec Fichiers XML : " + str(tte.nb_xml_zip) + "\n"

            sm.set_subject(tte.subject_mail + " OK ")
            sm.add_attachment(tte.log.get_log_path())
            sm.set_low_priority()
            sm.set_body(body)
            sm.go_mail()

            # Mail Cogeprint
            if tte.is_env_prod() is True:
                smcogeprint = Sendmail()
                body = MAIL_CECI
                body = body + "Bonjour,\n\nLes termes " + typeTermes + " sont disponible sur le répertoire d'échange " \
                                                                       "à l'adresse suivante : " + tte.nom_archive_zip_cogeprint + "\n\n"
                body = body + MAIL_W_PDF + str(tte.nb_pdf_zip) + "\n"
                body = body + MAIL_W_XML + str(tte.nb_xml_zip) + "\n"
                smcogeprint.set_body(body)
                smcogeprint.set_to(CONFIG['mailcogeprint']['cogeprintto'])
                smcogeprint.set_cc(CONFIG['mailcogeprint']['cogeprintcc'])
                smcogeprint.set_subject(tte.subject_mail + " - Dépôt zip pour traitement ", externe=True)
                smcogeprint.go_mail()

        '''# transmission des index pour stockage et analyse
        for files in os.listdir(os.path.join(tte.workingdir, "01_Composition")):
            if files.endswith(".ind"):
                shutil.copy(os.path.join(tte.workingdir, "01_Composition", files), "D:\\travail")'''
        # tte.purge_working_dir()

    except Exception as ex:
        tte.logwrite.error("FATAL ERROR : " + str(ex))
        body = "Traitement des termes terminé anormalement"

        sm = Sendmail()
        sm.set_subject(tte.subject_mail + " KO ")
        sm.add_attachment(tte.log.get_log_path())
        sm.set_high_priority()
        sm.set_body(body)
        sm.go_mail()
        sys.exit(-1)

    except KeyboardInterrupt:
        tte.logwrite.error("Interruption manuelle")

    # Mise à jour de l'écran de supervision <ip>:9100
    aogtools.maj_supervision(tte.appname)
