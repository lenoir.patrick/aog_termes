#!python
# coding: utf-8
#

from shutil import copyfile
import os


def maj_mentions_legales(id, libelle, ml_file):
    """Mets à jour le fichier des mentions légales avant traitement

    Args:
        id: Id de la mention légale
        libelle: libelle à mettre à jour
        ml_file: fichier à mettre jour
    """
    ml_copy_file = ml_file.replace(".tab", "_copy.tab")
    try:

        if os.path.isfile(ml_file) is True:
            copyfile(ml_file, ml_copy_file)

        fd_cible = open(ml_file, "w", encoding='ansi')
        fd_lecture = open(ml_copy_file, "r", encoding='ansi')
        flag_update = 0

        for line_ml in fd_lecture.read().splitlines():
            if line_ml != '':
                if line_ml.split('\t')[0] == id:
                    fd_cible.write(id + '\t' + libelle + '\n')
                    flag_update = 1
                else:
                    fd_cible.write(line_ml+'\n')
        
        if flag_update == 0:
            fd_cible.write(id+'\t'+libelle+'\n')

        fd_cible.close()
        fd_lecture.close()

        if os.path.isfile(ml_copy_file) is True:
            os.remove(ml_copy_file)
    except (Exception, ):
        pass
