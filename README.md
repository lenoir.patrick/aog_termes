# Pré-requis
> Les dépots suivants sont nécessaires à l'éxecution des termes
```
├── AOG_DOF_Toolbox (boite à outils des applis éditique AssurOne)
├── HCS Ressources (Les ressources dynamiques d'AssurOne)
│  ├──logos
│  ├──mentions légales
│  ├──paramétrage
```

# Introduction 
> Application de traitement des termes. Elle effectue toutes les tâches à la suite, il n'est plus nécessaire d'effectuer des opérations manuelles.
> Par défaut, chaque traitement va aller récupérer dans au bon endroit le dernier répertoire créé contenant des fichiers à traiter.

| **Script**                 | **Description**                                                         |
|----------------------------|-------------------------------------------------------------------------|
| **launch_AIG.cmd**         | Pour lancer le traitement des termes AIG                                | 
| **launch_STD.cmd**         | Pour lancer le traitement des termes standard                           | 
| **launch_ATTSCO.cmd**      | Pour lancer le traitement des attestations scolaires                    | 
| **launch_SANTE.cmd**       | Pour lancer le traitement des termes santé de fin d'année               |
| **launch_ATTSCO_TEST.cmd** | Pour lancer le traitement du fichier de tests des attestations scolaires |
| **launch_SANTE_TEST.cmd**  | Pour lancer le traitement du fichier de tests des termes santé          |
| **launch_STD_TEST.cmd**    | Pour lancer le traitement du fichier de tests des termes standard       |

# Paramétrage de `appsetting.json`
| **Champ**    | **Description**                                                                         |
|--------------|-----------------------------------------------------------------------------------------|
| **validBAT** | Ecart entre l'envoie d'un mail de validation lors de la production                      |
| **prod**     | Mode d'info des logs (mis à jour automatiquement par DevOps) debug (info/warning/error) |
| **purge**    | boolean pour purger le répertoire de travail                                            | 
| **ENV**      | Environnement de travail (mis à jour automatiquement par DevOps)                        | 

## dirs
| **Champ**         | **Description**                                                        |
|-------------------|------------------------------------------------------------------------|
| **input_dir_AIG** | Répertoire où se trouve les fichiers AIG à traiter                     |
| **input_dir_STD** | Répertoire où se trouve les fichiers à traiter                         |
| **output_dir**    | Répertoire où sont déposés les fichiers zip à destination de Cogeprint |

## composition
> TO DO : Platform/opInstallDir ? / input_dir/batchdir ?

| **Champ**             | **Description**                                                    |
|-----------------------|--------------------------------------------------------------------|
| **Platform**          | Répertoire d'installation de PlatForm                              |
| **opWD**              | Répertoire opWD de composition                                     |
| **cheminLogos**       | Répertoire des ressources images                                   |
| **cheminSignatures**  | Répertoire des ressources images signatures                        |
| **cheminParams**      | Répertoires des tables de paramétrage (logos, signatures, ...)     |
| **PYTHONHOME**        | Répertoire d'instance de Python à utiliser pour la composition     |
| **opInstallDir**      | Répertoire où se trouvent les moteurs Sefas                        |
| **input_dir**         |                                                                    |
| **batchPath**         |                                                                    |
| **hcsressourcespath** | Répertoire des ressources spécifiques à HCS (techsort, macros.tab) |
| **opTmpDir**          | Répertoire temporaire de traitement                                |

## report
> NOT USED

## mailcogeprint
> Les variables sont renseignées dans le pipeline pour la PRODUCTION

| **Champ**       | **Description**                                                 |
|-----------------|-----------------------------------------------------------------|
| **cogeprintto** | Destinataire principal du mail                                  |
| **cogeprintcc** | Destinataire en copie du mail (editique-ccm@assurone-group.com) |
 
## mailtechfactory
> Les variables sont renseignées dans le pipeline pour la PRODUCTION
> 
| **Champ** | **Description**                    |
|-----------|------------------------------------|
| **toba**  | Business Analyst à mettre en copie |
| **to**    | Destinataire principal du mail     |

``` json
{
    "dirs": {
        "input_dir_AIG": "\\\\A1-SRV-05\\Sauvegardes\\Extractions_CORTEX\\TERMES\\TERMES CORTEX\\AIG\\Archives",
        "input_dir_STD": "\\\\A1-SRV-05\\Assurone\\Direction Informatique\\GMC - projet editique\\resultats",
        "output_dir": "D:\\temp",
        "archive_dir": "D:\\temp"
    },
    "composition": {
        "Platform": "D:\\HCS\\Platform",
        "opWD": "D:\\referentielEditique",
        "cheminLogos": "D:\\\\ressources\\images\\logos\\",
        "cheminSignatures": "D:\\\\ressources\\images\\signatures\\",
        "cheminParams": "D:\\ressources\\params\\",
        "PYTHONHOME": "C:\\Python37",
        "opInstallDir": "D:\\HCS\\Platform\\bin\\backstage\\windows",
        "input_dir": "D:\\compositionEditique\\batchSefas\\entree",
        "batchPath": "D:\\compositionEditique\\batchSefas",
        "hcsressourcespath": "D:\\ressources\\hcs\\",
        "opTmpDir": "D:\\travail"
    },
    "mailcogeprint": {
        "cogeprintto": "plenoir@assurone-group.com",
        "cogeprintcc": "plenoir@assurone-group.com"
    },
    "mailtechfactory": {
        "to2": "termes_cortex_ae_cv@assurone-group.com",
        "toba": "jcedolin@assurone-group.com",
        "to": "plenoir@assurone-group.com"
    },
    "mailqualitegestion": {
        "to": "plenoir@assurone-group.com"
    },
    "mentions_legales": {
        "tab_file": "D:\\ressources\\params\\MentionsLegales.tab",
        "update": true
    },
    "prod": "info",
    "purge": true,
    "ENV": "PROD"
}
```