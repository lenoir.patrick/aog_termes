# coding: utf-8
import os
import AOG.tools as aogtools


def encodage_fichier(fichier_entree, encodage_fichier_entree, fichier_sortie, encodage_fichier_sortie):
    """Encodage du fichier en entree vers le fichier en sortie avec l'encodage souhaite

    Args:
        fichier_entree: fichier en entree, idealement chemin absolu
        encodage_fichier_entree: encodage du fichier en entree
        fichier_sortie: fichier en sortie, idealement chemin absolu
        encodage_fichier_sortie: encodage du fichier en sortie
    """
    fd_i = open(fichier_entree, 'r', encoding=encodage_fichier_entree)
    fd_out = open(fichier_sortie, 'w', encoding=encodage_fichier_sortie)
    s = fd_i.read()
    fd_out.write(s)
    fd_out.close()
    fd_i.close()


def remplace_car_file(fichier_entree, encodage_fichier_entree, car_cible, car_remplacement):
    """Remplacement d'un caractere dans un fichier

    Args
        fichier_entree:  fichier en entree, idealement chemin absolu
        encodage_fichier_entree:  encodage du fichier en entree
        car_cible:  le caractere à remplacer
        car_remplacement:  le caractere de remplacement
    """
    fichier_intermediaire = fichier_entree + "_tmp_" + aogtools.get_time_stamp()
    
    fd_r_i = open(fichier_entree, 'r', encoding=encodage_fichier_entree)
    fd_w_intermediaire = open(fichier_intermediaire, 'w', encoding=encodage_fichier_entree)
    # on écrit chacune des lignes du fichier de base dans les fichiers en sortie en remplacant le caractere impie
    for line in fd_r_i.readlines():
        fd_w_intermediaire.write(line.replace(car_cible, car_remplacement))
    
    fd_r_i.close()
    fd_w_intermediaire.close()
    
    encodage_fichier(fichier_intermediaire, encodage_fichier_entree, fichier_entree, encodage_fichier_entree)
    
    os.remove(fichier_intermediaire)

# TEST OK
# remplace_car_file("testAC.csv", "ANSI", '\t', ' ')
# encodage_fichier("testAC.csv", "ANSI", "testAC_utf8.csv", "utf-8")
